# Scopus API
# install.packages("rscopus")
library(rscopus)
library(openxlsx) # importing xlsx
library(jsonlite) # reading api body
library(httr)     # pulling api request with GET function

authors <- read.xlsx('../researcherAUF.xlsx')
# authors <- read.xlsx('./authors.xlsx')
# Install and load the rscopus package

# Set your API key
api_key <- "b7c5a3dbe3fa5de5c98d0643b1c1c67f"    # from Elsevier
set_api_key(api_key) # is bound to the domain of your institution!, e.g. set up VPN system('sudo wg-quick up wg')

# rscopus package examples
get_author_info(au_id = 8757943900)
# get_complete_author_info(au_id = 8757943900, api_key = api_key)
get_complete_author_info(last_name = 'Eichler-Löbermann', first_name = 'Bettina', affil_name = 'Universität Rostock OR Dummerstorf')

# paste0("AUTHOR-NAME(", author_name, ") AND af_id(", 60003615, ")")

#' Note
#' A limited number of field codes are available.
#' Enter field codes in upper or lower case.
#' Make sure to use the correct field code spelling, including hyphens.
#' Not all documents contain all fields. Searching specific fields may prevent some articles from appearing in your search results
#' @ = included in ALL fields search
#' Searches the following fields: ABS, AFFIL, ARTNUM, AUTH, AUTHCOLLAB, CHEM, CODEN, CONF, DOI, EDITOR, ISBN, ISSN, ISSUE, KEY, LANGUAGE, MANUFACTURER, PUBLISHER, PUBYEAR, REF, SEQBANK, SEQNUMBER, SRCTITLE, VOLUME, and TITLE.
#' ALL("heart attack") returns documents with "heart attack" in any of the fields listed.
# AUTHOR-NAME(carrera, s) returns
# AFFILCITY(beijing) returns documents where "beijing" is the city in the author
# KEY(cold) AND KEY(rhinovirus) AND NOT KEY(influenza)

#https://www.scopus.com/results/authorNamesList.uri?sort=count-f&src=al&affilName=Rostock&sid=2014ea4f78a9001d4045eaaccc296f5a&sot=al&sdt=al&sl=62&s=AUTHLASTNAME%28Jansen%29+AND+AUTHFIRST%28Florian%29+AND+AFFIL%28Rostock%29&st1=Jansen&st2=Florian&orcidId=&selectionPageSearch=anl&reselectAuthor=false&activeFlag=true&showDocument=false&resultsPerPage=20&offset=1&jtp=false&currentPage=1&previousSelectionCount=0&tooManySelections=false&previousResultCount=0&authSubject=LFSC&authSubject=HLSC&authSubject=PHSC&authSubject=SOSC&exactAuthorSearch=false&showFullList=false&authorPreferredName=&origin=searchauthorfreelookup&affiliationId=&txGid=d2b608e4f3ae32d69b1f9d0425987836

#  working examples, see https://dev.elsevier.com/sc_search_tips.html
# http://api.elsevier.com/content/search/scopus?query=DOI%28%2210.1021%2Fes052595%2B%22%29&apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f
# http://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=DOI%28%2210.1021%2Fes052595%2B%22%29
# https://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=heart%20attack
# https://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=KEY%28cat+AND+dog+AND+NOT+rodent+OR+mouse%29  # KEY(cat AND dog AND NOT rodent OR mouse)
# https://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=AUTHOR-NAME%28%20Florian%20Jansen%29
# https://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=AUTHOR-NAME%28Florian%20Jansen%29%20AND%20AFFILCITY%28Rostock%29
# URLencode(paste0("https://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=AUTHOR-NAME(", author_name, ") AND AFFILCITY(", city, ")"))
# URLencode(paste0("https://api.elsevier.com/content/search/scopus?apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f&query=AUTHOR-NAME(", author_name, ") AND AFFILCITY(", city, ")"))
# URLencode(paste0("https://api.elsevier.com/content/author?author_id=8757943900&view=metrics&apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f"))
# URLencode(paste0("https://api.elsevier.com/content/search/scopus?query=AUTHOR-NAME(", author_name, ") AND AFFIL(", city, ")&apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f"))

# eid: The unique identifier for each document in Scopus.
# doi: The Digital Object Identifier for the document.
# pii: The Publisher Item Identifier for the document.
# pubmed_id: The PubMed ID for the document.
# title: The title of the document.
# subtype: The subtype of the document.
# subtypeDescription: A description of the subtype of the document.
# creator: The creator or author of the document.
# afid: The affiliation ID of the creator or author.
# affilname: The name of the affiliation of the creator or author.
# affiliation_city: The city of the affiliation of the creator or author.
# affiliation_country: The country of the affiliation of the creator or author.
# author_count: The number of authors for the document.
# author_names: The names of the authors for the document.
# author_ids: The Scopus IDs of the authors for the document.
# author_afids: The affiliation IDs of the authors for the document.
# coverDate: The cover date of the document.
# coverDisplayDate: The formatted cover date of the document.
# publicationName: The name of the publication where the document was published.
# issn: The International Standard Serial Number (ISSN) of the publication.
# source_id: The Scopus ID of the source (journal or conference) where the document was published.
# eIssn: The Electronic International Standard Serial Number (EISSN) of the publication.
# aggregationType: The type of aggregation for the document (e.g., journal, conference).
# volume: The volume of the publication.
# issueIdentifier: The issue identifier of the publication.
# article_number: The article number of the document.
# pageRange: The page range of the document.
# description: The description or abstract of the document.
# authkeywords: The author keywords associated with the document.
# citedby_count: The number of times the document has been cited.
# openaccess: Indicates whether the document is open access or not.
# freetoread: Indicates whether the document is freely available to read or not.
# freetoreadLabel: A label for the free-to-read status of the document.
# fund_acr: The acronym of the funding source for the document.
# fund_no: The funding number associated with the document.
# fund_sponsor: The sponsor of the funding for the document.




### Loop 1: Identify AUF authors by Scopus author_id ####
for (i in 1:nrow(authors)){
  print(authors$NAME[i])
  lastname <- sub(',', '', stringr::word(authors$NAME[i], 1))
  firstname <- stringr::word(authors$NAME[i], 2)
#  query <- URLencode(paste0("https://api.elsevier.com/content/search/author?query=authlast(", lastname, ") and authfirst(", firstname, ") and af_id('60003615 OR 60072318')&apiKey=", api_key)) # only one id per request?
  query <- URLencode(paste0("https://api.elsevier.com/content/search/author?query=authlast(", lastname, ") and authfirst(", firstname, ") and affil('Universität Rostock OR Dummerstorf')&apiKey=", api_key))
  res <- try(GET(query, config = add_headers(Accept= 'application/json')))
  # fromJSON(rawToChar(res$content))
  if(class(res) == 'response'){
    tmp <- fromJSON(rawToChar(res$content))
    if(!is.null(tmp$`search-results`$entry$`dc:identifier`)){
      authors[i, 'au_id'] <- paste0(sub("AUTHOR_ID:", "", tmp$`search-results`$entry$`dc:identifier`), collapse = ', ')
      authors[i, 'curr_affil_institution_scopus'] <- paste0(tmp$`search-results`$entry$`affiliation-current`$`affiliation-name`, collapse = ', ')
      authors[i, 'document-count_scopus'] <- paste0(tmp$`search-results`$entry$`document-count`, collapse = ', ')
    } else {
      authors$au_id[i] <- "not found"
      authors$curr_affil_institution_scopus[i] <- "not found"
    }
    rm(tmp)
  } else authors$au_id[i] <- "error"
}

cat('\n')
print("Die Ergebnisse in authors$au_id erfordern eine händische Überprüfung!! \n
      Die zu verwendenden ID's werden im Folgenden aus der Spalte SCOPUS entnommen.")
cat('\n')


### Loop 2: Publications from last year including h-score (sum) ####
authors$publication_journal <- NA
authors$`h.index_year` <- NA
for (i in 1:nrow(authors)){
  print(authors$NAME[i])
  if(!authors$SCOPUS[i] == "not found"){
    au_id <- authors$SCOPUS[i]   # strsplit(authors$au_id[i], ', ')[[1]][1]
    query <- URLencode(paste0("https://api.elsevier.com/content/search/scopus?query=AU-ID(", au_id, ")&apiKey=", api_key))
    tes <- GET(query, config = add_headers(Accept= 'application/json'))
    if(class(tes) == 'response'){
      tmp <- fromJSON(rawToChar(tes$content))$`search-results`$entry
      if(!is.null(tmp$`dc:title`)){
        tmp <- tmp[substr(tmp$`prism:coverDate`, 1, 4) == format(Sys.time(), "%Y"), ]
        if(nrow(tmp) > 0){
          tmp$journalhindex <- NA
          for(j in 1:nrow(tmp)) {
          # https://api.elsevier.com/content/serial/title?title=Applied%20Vegetation%20Science&apiKey=b7c5a3dbe3fa5de5c98d0643b1c1c67f
          hj <- fromJSON(rawToChar(GET(URLencode(paste0("https://api.elsevier.com/content/serial/title?title=", tmp$`prism:publicationName`[j], "&apiKey=", api_key)), config = add_headers(Accept= 'application/json'))$content))
          h <- hj$`serial-metadata-response`$entry$`citeScoreYearInfoList`$`citeScoreCurrentMetric`[1]
          tmp$journalhindex[j] <- if(is.null(h)) 0 else h
          rm(hj)
          }
        pub_list <- paste(tmp$`dc:creator`, ' (', substr(tmp$`prism:coverDate`, 1,4), ') ', tmp$`dc:title`, ', ', tmp$`prism:publicationName`, ' ', tmp$`prism:volume`, ', ', tmp$`prism:pageRange`, '. h-index: ',  tmp$journalhindex, '.\n', sep = '')
        authors$publication_journal[i] <- toString(pub_list)
        authors$`h.index_year`[i] <- sum(as.numeric(tmp$journalhindex))
        rm(pub_list)
        }
      }
      else authors$publication_journal[i] <- "not found"
      rm(tmp)
    }
    else authors$publication_journal[i] <- "error"
  }
}

View(authors[, c(-3,-4,-5)])

### Loop 3: for author metrics: h-score, document-count and citation-count ####
for (i in 1:nrow(authors)){
  print(authors$NAME[i])
  if(!authors$SCOPUS[i] == "not found"){
    au_id <- authors$SCOPUS[i]
    query <- URLencode(paste0("https://api.elsevier.com/content/author?author_id=", au_id, "&view=metrics&apiKey=", api_key))
    pes <- GET(query, config = add_headers(Accept= 'application/json'))
    if(class(pes) == 'response'){
      tmp <- fromJSON(rawToChar(pes$content))
      if(!is.null(tmp$`author-retrieval-response`$coredata$`citation-count`)){
        authors$count_doc[i] <- tmp$`author-retrieval-response`$coredata$`document-count`
        authors$count_citation[i] <- tmp$`author-retrieval-response`$coredata$`citation-count`
        authors$count_h[i] <- tmp$`author-retrieval-response`$`h-index`
      } else {
        authors$count_h[i] <- NA
        authors$count_doc[i] <- NA
        authors$count_citation[i] <- NA
      }
      rm(tmp)
    }
  }
}
# The Elsevier API is slow, therefore we repeat for missings 
for (i in 1:nrow(authors)){
    if(is.na(authors$count_doc[i])) {
    print(authors$NAME[i])
    au_id <- authors$SCOPUS[i]
    query <- URLencode(paste0("https://api.elsevier.com/content/author?author_id=", au_id, "&view=metrics&apiKey=", api_key))
    pes <- GET(query, config = add_headers(Accept= 'application/json'))
    if(class(pes) == 'response'){
      tmp <- fromJSON(rawToChar(pes$content))
      if(!is.null(tmp$`author-retrieval-response`$coredata$`citation-count`)){
        authors$count_doc[i] <- tmp$`author-retrieval-response`$coredata$`document-count`
        authors$count_citation[i] <- tmp$`author-retrieval-response`$coredata$`citation-count`
        authors$count_h[i] <- tmp$`author-retrieval-response`$`h-index`
      } else {
        authors$count_h[i] <- NA
        authors$count_doc[i] <- NA
        authors$count_citation[i] <- NA
      }
      rm(tmp)
    }
  }
}


authors$count_h <- as.integer(authors$count_h)
authors$count_doc <- as.integer(authors$count_doc)
authors$count_citation <- as.integer(authors$count_citation)

View(authors[, c(-3,-4,-5,-6)])

## finally exporting back to xlsx
write.xlsx(authors, 'authors.xlsx')

