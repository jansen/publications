library(scholar)
id <- 'SQTXmrAAAAAJ&hl'
l <- get_profile(id)
p <- get_publications(id)
p[p$year == '2020' & !is.na(p$year), ]

## Plot citation trend
# library(ggplot2)
# ggplot(ct, aes(year, cites)) + geom_line() + geom_point()

coauthor_network <- get_coauthors(id, n_coauthors = 7)
plot_coauthors(coauthor_network)
