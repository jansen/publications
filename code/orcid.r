
library('rorcid')
orcid_auth()
as.orcid('0000-0002-0331-5185')
out <- orcid_works(orcid="0000-0002-0331-5185")[[1]][[1]]
# names(out)
out[out$`publication-date.year.value` == '2022', c("type",  "title.title.value", "journal-title", "url.value")]
