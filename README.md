# Publikationen bei Scopus und Co abholen

Publikationslisten von WOS, Scopus, ORCID, und Google Scholar per R code abrufen, 

* um AUF-LOM Berechnungen zu automatisieren, 
* die Vollständigkeit der Datenbanken zu kontrollieren
* ggf. mit der UB zu verhandeln, wie die Uni-Datenbank beschleunigt werden kann
* und was uns sonst noch so einfällt.